# CS-E4640 - MLLib
Solution for the MLLib assignment of CS-E4640 Big Data Platforms taught at Aalto University during the fall semester of 2018.

The assignment regards using Apache Spark's machine learning library MLLib to apply machine learning on vast datasets applying distributed computing.
