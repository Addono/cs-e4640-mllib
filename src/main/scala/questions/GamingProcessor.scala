package questions

import org.apache.spark.sql.{DataFrame, Row, SparkSession}
import org.apache.spark.ml.feature.StringIndexer
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.ml.feature.StandardScaler
import org.apache.spark.ml.classification.{LogisticRegression, LogisticRegressionModel}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.types.{DoubleType, StringType, StructField, StructType}


/**
  * GamingProcessor is used to predict if the user is a subscriber.
  * You can find the data files from /resources/gaming_data.
  * Data schema is https://github.com/cloudwicklabs/generator/wiki/OSGE-Schema
  * (first table)
  *
  * Use Spark's machine learning library mllib's logistic regression algorithm.
  * https://spark.apache.org/docs/latest/ml-classification-regression.html#binomial-logistic-regression
  *
  * Use these features for training your model:
  *   - gender
  *   - age
  *   - country
  *   - friend_count
  *   - lifetime
  *   - citygame_played
  *   - pictionarygame_played
  *   - scramblegame_played
  *   - snipergame_played
  *
  * -paid_subscriber(this is the feature to predict)
  *
  * The data contains categorical features, so you need to
  * change them accordingly.
  * https://spark.apache.org/docs/latest/ml-features.html
  *
  *
  *
  */
class GamingProcessor() {


  // these parameters can be changed
  val spark = SparkSession.builder
    .master("local")
    .appName("gaming")
    .config("spark.driver.memory", "5g")
    .config("spark.executor.memory", "2g")
    .getOrCreate()

  import spark.implicits._


  /**
    * convert creates a dataframe, removes unnecessary colums and converts the rest to right format.
    * Data schema:
    *   - gender: Double (1 if male else 0)
    *   - age: Double
    *   - country: String
    *   - friend_count: Double
    *   - lifetime: Double
    *   - game1: Double (citygame_played)
    *   - game2: Double (pictionarygame_played)
    *   - game3: Double (scramblegame_played)
    *   - game4: Double (snipergame_played)
    *   - paid_customer: Double (1 if yes else 0)
    *
    * @param path to file
    * @return converted DataFrame
    */
  def convert(path: String): DataFrame = {
    val columns = StructType(Array(StructField("gender", DoubleType, false),
      StructField("age", DoubleType, false),
      StructField("country", StringType, true),
      StructField("friend_count", DoubleType, false),
      StructField("lifetime", DoubleType, false),
      StructField("game1", DoubleType, false),
      StructField("game2", DoubleType, false),
      StructField("game3", DoubleType, false),
      StructField("game4", DoubleType, false),
      StructField("paid_customer", DoubleType, false)))

    spark.read
      .option("header", "false")
      .csv(path)
      .select("_c3", "_c4", "_c6", "_c8", "_c9", "_c10", "_c11", "_c12", "_c13", "_c15")
      .withColumnRenamed("_c3", "gender")
      .withColumnRenamed("_c4", "age")
      .withColumnRenamed("_c6", "country")
      .withColumnRenamed("_c8", "friend_count")
      .withColumnRenamed("_c9", "lifetime")
      .withColumnRenamed("_c10", "game1")
      .withColumnRenamed("_c11", "game2")
      .withColumnRenamed("_c12", "game3")
      .withColumnRenamed("_c13", "game4")
      .withColumnRenamed("_c15", "paid_customer")
      .withColumn("gender", $"gender".isin("male").cast("double"))
      .withColumn("age", $"age".cast("double"))
      .withColumn("friend_count", $"friend_count".cast("double"))
      .withColumn("lifetime", $"lifetime".cast("double"))
      .withColumn("game1", $"game1".cast("double"))
      .withColumn("game2", $"game2".cast("double"))
      .withColumn("game3", $"game3".cast("double"))
      .withColumn("game4", $"game4".cast("double"))
      .withColumn("paid_customer", $"paid_customer".isin("yes").cast("double"))
  }

  /**
    * indexer converts categorical features into doubles.
    * https://spark.apache.org/docs/latest/ml-features.html
    * 'country' is the only categorical feature.
    * After these modifications schema should be:
    *
    *   - gender: Double (1 if male else 0)
    *   - age: Double
    *   - country: String
    *   - friend_count: Double
    *   - lifetime: Double
    *   - game1: Double (citygame_played)
    *   - game2: Double (pictionarygame_played)
    *   - game3: Double (scramblegame_played)
    *   - game4: Double (snipergame_played)
    *   - paid_customer: Double (1 if yes else 0)
    *   - country_index: Double
    *
    * @param df DataFrame
    * @return Dataframe
    */
  def indexer(df: DataFrame): DataFrame = {
    val indexer = new StringIndexer()
      .setInputCol("country")
      .setOutputCol("country_index")

    indexer.fit(df).transform(df)
  }

  /**
    * Combine features into one vector. Most mllib algorithms require this step
    * https://spark.apache.org/docs/latest/ml-features.html#vectorassembler
    * Column name should be 'features'
    *
    * @param Dataframe that is transformed using indexer
    * @return Dataframe
    */
  def featureAssembler(df: DataFrame): DataFrame = {
    val assembler = new VectorAssembler()
      .setInputCols(df.columns)
      .setOutputCol("features")

    assembler.transform(df)
  }

  /**
    * To improve performance data also need to be standardized, so use
    * https://spark.apache.org/docs/latest/ml-features.html#standardscaler
    *
    * @param Dataframe that is transformed using featureAssembler
    * @param  name     of the scaled feature vector (output column name)
    * @return Dataframe
    */
  def scaler(df: DataFrame, outputColName: String): DataFrame = {
    new StandardScaler()
      .setInputCol("features")
      .setOutputCol(outputColName)
      .fit(df)
      .transform(df)
  }

  /**
    * createModel creates a logistic regression model
    * When training, 5 iterations should be enough.
    *
    * @param training    dataframe
    * @param featuresCol name of the features columns
    * @param labelCol    name of the label col (paid_customer)
    * @param predCol     name of the prediction col
    * @return trained LogisticRegressionModel
    */
  def createModel(training: DataFrame, featuresCol: String, labelCol: String, predCol: String): LogisticRegressionModel = {
    val lr = new LogisticRegression()
      .setMaxIter(10)
      .setRegParam(0.3)
      .setElasticNetParam(0.8)
      .setLabelCol(labelCol)
      .setFeaturesCol(featuresCol)
      .setPredictionCol(predCol)

    lr.fit(training)
  }


  /**
    * Given a transformed and normalized dataset
    * this method predicts if the customer is going to
    * subscribe to the service.
    *
    * @param model         trained logistic regression model
    * @param dataToPredict normalized data for prediction
    * @return DataFrame predicted scores (1.0 == yes, 0.0 == no)
    */
  def predict(model: LogisticRegressionModel, dataToPredict: DataFrame): DataFrame = {
    model.evaluate(dataToPredict).predictions
  }

}

/**
  *
  * Change the student id
  */
object GamingProcessor {
  val studentId = "727684"
}
